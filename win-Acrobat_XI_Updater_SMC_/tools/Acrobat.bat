@Echo Off


Rem SMC Software Updater Acrobat XI Update
Rem Created by Jeremy Fife
Rem Created 5-31-17


Rem Set Download Save to Path
Set Install= C:\SMC_Tools\Temp
md %Install%

Rem Set Downlad URL
Set Download= http://ardownload.adobe.com/pub/adobe/acrobat/win/11.x/11.0.20/misc/AcrobatUpd11020.msp

Set Switches= /qn



Rem Start Download and Install 
bitsadmin.exe /transfer "Acrobat XI Update" %Download% %Install%\AcrobatUpd11020.msp

%Install%\AcrobatUpd11020.msp %Switches%

Del %Install% /F /Q



