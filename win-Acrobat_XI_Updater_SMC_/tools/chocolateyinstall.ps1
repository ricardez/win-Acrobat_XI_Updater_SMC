﻿
$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$fileLocation = Join-Path $toolsDir 'Acrobat.bat'
$url        = ''
$url64      = ''

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'EXE'
  url           = $url
  url64bit      = $url64
  file         = $fileLocation

  softwareName  = 'win-Acrobat_XI_Updater_SMC_*'

  checksum      = '1C7AB08FFA9783301E9EE5B7558AF571218AC2DC3E4FC31474450950DA6C148A'
  checksumType  = 'sha256'
  checksum64    = '1C7AB08FFA9783301E9EE5B7558AF571218AC2DC3E4FC31474450950DA6C148A'
  checksumType64= 'sha256'

 
  validExitCodes= @(0, 3010, 1641)
  silentArgs   = ''
}

Install-ChocolateyInstallPackage @packageArgs










    








